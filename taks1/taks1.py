import time
from concurrent.futures import ThreadPoolExecutor, wait, ALL_COMPLETED

import requests
import argparse


URL = 'http://jsonplaceholder.typicode.com/posts/'


def parse_arg():
    parser = argparse.ArgumentParser(description='HELLO, please write number for request (1-100). \n\t '
                                                 'For default n = 5')
    parser.add_argument('-n', default=5, type=int, help='available number: 1-100', dest='post_number')
    args = parser.parse_args()
    return args.post_number
    

def single_threaded(rnumber):
    _startTime = time.time()
    for number in range(1, rnumber):
        send_request(number)
    return time.time() - _startTime


def send_request(post_number):
    try:
        requests.get(URL + str(post_number))
    except Exception:
        print('send GET request from {}{} unsuccessful'.format(URL, post_number))


def multi_threaded(rnumber):
    _startTime = time.time()
    with ThreadPoolExecutor(max_workers=rnumber) as executor:
        send = {post_number: executor.submit(send_request, post_number) for post_number in range(1, rnumber)}
        wait(send.values(), return_when=ALL_COMPLETED)
    return time.time() - _startTime


if __name__ == "__main__":
    request_number = parse_arg()
    time_multi_threaded = multi_threaded(request_number)
    time_single_threaded = single_threaded(request_number)
    print("Parallel: {:.3f} sec".format(time_multi_threaded))
    print("Sequential: {:.3f} sec".format(time_single_threaded))
