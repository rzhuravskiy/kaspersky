from pcapfile import savefile

file = open('dump3.pcap', 'rb')
pcapfile = savefile.load_savefile(file, layers=2)


def parse_pcap_file():
    for packet in pcapfile.packets:
        if packet.packet.type == 2048:
            # IPv4
            src = packet.packet.payload.src.decode(encoding="utf-8")
            dst = packet.packet.payload.dst.decode(encoding="utf-8")
            print('[SrcIP: {src} - DstIP: {dst}]'.format(src=src, dst=dst))
        elif packet.packet.type == 34525:
            # IPv6
            src = packet.packet.src.decode(encoding="utf-8")
            dst = packet.packet.dst.decode(encoding="utf-8")
            print('[SrcIP: {src} - DstIP: {dst}]'.format(src=src, dst=dst))

        else:
            src = packet.packet.src.decode(encoding="utf-8")
            dst = packet.packet.dst.decode(encoding="utf-8")
            print('[SrcMAC: {src} - DstMAC: {dst}]'.format(src=src, dst=dst))


if __name__ == "__main__":
    parse_pcap_file()
